import React, { Component } from "react"
import { StyleSheet, Text, View, TouchableHighlight, Image, Button } from "react-native"
import { androidClientId, iosClientId } from "./superSecretKey"
import Expo from "expo"

export default class LotsOfStyles extends Component {

  constructor(props) {
    super(props)
    this.state = {
      signedIn: false,
      name: "",
      photoUrl: ""
    }
  }
  
  signIn = async () => {
    try {
      const result = await Expo.Google.logInAsync({
        androidClientId: androidClientId,

        //iosClientId: iosClientId, 

        scopes: ["profile", "email"]
      })

      if (result.type === "success") {
        this.setState({
          signedIn: true,
          name: result.user.name,
          photoUrl: result.user.photoUrl
        })
      } else {
        console.log("geannuleerd")
      }
    } catch (e) {
      console.log("error", e)
    }
  }
  
  render() {
    return (
      <View style={styles.container}>
        {this.state.signedIn ? (
          <LoggedInPage name={this.state.name} photoUrl={this.state.photoUrl} />
        ) : (
            <LoginPage signIn={this.signIn} />
          )}
      </View>
    );
  }
}
const LoginPage = props => {
  return (
    <View style={styles.container}>
      <View>
        <Image source={require('./Temperature.jpg')} style={styles.iconImage}/>
      </View>
      <View>
        <Text style={styles.headline}>Project 5/6</Text>
      </View>
      <View>
        <Text style={styles.headline}>Team 56</Text>
      </View >
        <Text style={styles.header}>Log in met uw Google-account</Text>
      {/* <Button
        icon={{ name: 'code' }}
        backgroundColor = '#03A9F4'
        buttonStyle={{ borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0 }}
        title="Log in met Google" 
        onPress={() => props.signIn()} 
      /> */}
      
        <TouchableHighlight onPress={() => props.signIn()}>
          <Image
            style={styles.buttonTouchableHighlight}
            source={require('./signin-button.png')}
        />
        </TouchableHighlight>
    

    </View>
  )
}

const LoggedInPage = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Welkom: {props.name}</Text>
      <Image style={styles.image} source={{ uri: props.photoUrl }} />
    </View>
  )
}

const styles = StyleSheet.create({

    container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center"
    },

    header: {
      textAlign: 'center',
      fontSize: 20,
      height: 35,
    },

    buttonTouchableHighlight: {
      //alignItems: 'center',
      backgroundColor: '#03A9F4',
      //width: 50,
    },

    iconImage: {
      width: 200,
      height: 200,
    },
    
    image: {
      marginTop: 15,
      width: 150,
      height: 150,
      borderColor: "rgba(0,0,0,0.2)",
      borderWidth: 3,
      borderRadius: 150
    },

  headline: {
    textAlign: 'center', // <-- the magic
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 0,
    height: 50,
    color: 'black',
  }

});
